package com.iretail.controller;

import com.iretail.entity.CustomerEntity;
import com.iretail.service.CustomerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/customers")
public class CustomerController {
    @Autowired
    CustomerServiceImpl customerService;

    @GetMapping("/{id}")
    public CustomerEntity get(@PathVariable Long id) throws Throwable {
        return customerService.get(id);
    }

    @PostMapping
    public CustomerEntity create(@RequestBody CustomerEntity customerEntity) {
        return customerService.creat(customerEntity);
    }

    @PutMapping("/{id}")
    public CustomerEntity update(@PathVariable Long id, @RequestBody CustomerEntity request) throws Throwable {
        return customerService.update(id, request);

    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        customerService.delete(id);
    }
}
