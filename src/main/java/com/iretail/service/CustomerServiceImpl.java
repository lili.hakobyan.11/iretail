package com.iretail.service;

import com.iretail.entity.CustomerEntity;
import com.iretail.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class CustomerServiceImpl {

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerEntity get(Long id) throws Throwable {
        return customerRepository.findById(id)
                .orElseThrow((Supplier<Throwable>) () -> new RuntimeException("Not Existing custmer with ID " + id));
    }

    public CustomerEntity creat(CustomerEntity customerEntity) {
        return customerRepository.save(customerEntity);
    }

    public CustomerEntity update(Long id, CustomerEntity customerEntity) throws Throwable {
        CustomerEntity exictingCustomer = customerRepository.findById(id)
                .orElseThrow((Supplier<Throwable>) () -> new RuntimeException("Not Existing custmer with ID " + id));
        exictingCustomer.setName(customerEntity.getName());
        customerRepository.save(exictingCustomer);
        return exictingCustomer;
    }

    public void delete(Long id) {

        customerRepository.deleteById(id);

    }
}
