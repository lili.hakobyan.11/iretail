package com.iretail.listener;


import com.iretail.entity.CustomerEntity;
import com.iretail.entity.OrderEntity;
import com.iretail.entity.ProductEntity;
import com.iretail.entity.RestaurantEntity;
import com.iretail.repositories.CustomerRepository;
import com.iretail.repositories.OrderRepository;
import com.iretail.repositories.ProductRepository;
import com.iretail.repositories.RestaurantRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ApplicationEventListener {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @EventListener({ContextRefreshedEvent.class})
    public void onContextRefreshedEvent() {

        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setName("Marta");
        ProductEntity productEntity = new ProductEntity();
        productEntity.setName("ClubSandwich");
        RestaurantEntity restaurantEntity = new RestaurantEntity();
        restaurantEntity.setName("Friday");
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setName("EveningOrder");

        CustomerEntity customerSaved = customerRepository.findById(1L)
                .orElse(null);
        customerSaved.setName("Lilit best mama in the world");
        customerRepository.save(customerSaved);


        customerRepository.save(customerEntity);
        productRepository.save(productEntity);
        orderRepository.save(orderEntity);
        restaurantRepository.save(restaurantEntity);


        List<CustomerEntity> customers = customerRepository.findAll();

        for (CustomerEntity customer :
                customers) {
            System.out.println(customer);
        }
    }
}
